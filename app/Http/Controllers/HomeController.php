<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\MyClass;

class HomeController extends Controller
{
    //
    public function index() {

        //return view( 'welcome' );

        $sc = new MyClass();
        $res = $sc->getVar(2);

        $messages = Message::all();
/*
        foreach( $messages as $message ){
            echo $message->title;
            echo "<br>";
        }; die;
*/
        //dd($messages);

        return view( 'home', [
            'messages' => $messages,
            'res' => $res,
        ]);


    }
}
