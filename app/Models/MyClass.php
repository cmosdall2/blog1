<?php

namespace App\Models;

class MyClass
{
    // property declaration
    private $pi = 3.141592;

    // method declaration
    public function getVar($n) {
        return $this->getPi() * $n;
        //echo $this->var;
    }
    private function getPi() {
        return $this->pi;
    }
}
?>