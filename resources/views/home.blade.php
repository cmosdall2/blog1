@extends('master')

@section('title', 'Homepage')

@section('content')

    <H1>Content!</H1>

    Recent Messages {{ date('Y-m-d H:i:s') }}<br>
    <h4>{{ $res }}</h4>

    <form action="/create" method="post">
        {{-- @csrf --}}
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="content" placeholder="Content">
        {{ csrf_field() }}
        <button type="submit">Submit</button>
    </form>

    <ul>

        @foreach( $messages as $message )
        
            <li>
                <strong>{{ $message->title }}</strong>
                <br>
                {{ $message->content }}
                <br>
                {{ $message->created_at->diffForHumans() }}
                <br>
                <a href="/message/{{ $message->id }}">view</a>
            </li>
        
        @endforeach


    </ul>

@endsection


