@extends('master')

@section('title', $message->title )

@section('content')

    <H2>{{ $message->title }}...</H2>
    <p>{{ $message->content }}</p>

@endsection


